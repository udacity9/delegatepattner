import Foundation

print("hellow")

enum Genre: String {
    case country, blues, folk
}

struct Artist {
    let name: String
    var primaryGenre: Genre?
}

struct Song {
    let title: String
    let released: Int
    var artist: Artist?
}

func getArtistGenre(song: Song) -> String {
    var response :String?
    if let genre = song.artist?.primaryGenre {
        response = genre.rawValue
        
    }
    return response ?? "falhou"
}

func main(){
    let song: Song = Song(title: "amanha", released: 1993, artist: nil)
    let song2: Song = Song(title: "amanha", released: 1993, artist: Artist(name: "paul"))
    let song3: Song = Song(title: "amanha", released: 1993, artist: Artist(name: "jonh", primaryGenre: .blues))
    
    print(getArtistGenre(song: song3))
}
//e preciso chamar a funcao para ver seu resultado
main()
func getArtistGenre1(song: Song) -> String {
  
    guard let response = song.artist?.primaryGenre?.rawValue else { return ""}
    
    return response
}
